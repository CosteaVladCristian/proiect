package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "MessageEntity.getAllUnreadMessages", query = "SELECT c FROM MessageEntity c WHERE"
				+ " c.isRead = false and c.sender=:sender and c.user.id=:userId"),
		@NamedQuery(name = "MessageEntity.findbyUserId", query = "SELECT c FROM MessageEntity c WHERE c.user.id=:userId"),
		@NamedQuery(name = "MessageEntity.getSaAllUnreadMessages", query = "SELECT c FROM MessageEntity c WHERE c.isRead = false and c.sender=:sender") })
@Entity
@Table(name = "MESSAGE_TABLE")
public class MessageEntity {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private long id;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "MESSAGE")
	private String message;

	@Column(name = "IS_READ")
	private boolean isRead;

	@Column(name = "SENDER")
	private String sender;

	@Column(name = "MESSAGE_TS")
	private long messageTs;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private UserEntity user;

	@ManyToOne
	@JoinColumn(name = "CAR_ID")
	private CarEntity car;

	public MessageEntity() {

	}

	public MessageEntity(String title, String message, boolean isRead, String sender, UserEntity user) {
		super();
		this.title = title;
		this.message = message;
		this.isRead = isRead;
		this.sender = sender;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRead() {
		return isRead;
	}

	public void setRead(boolean isRead) {
		this.isRead = isRead;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public long getMessageTs() {
		return messageTs;
	}

	public void setMessageTs(long messageTs) {
		this.messageTs = messageTs;
	}

	public CarEntity getCar() {
		return car;
	}

	public void setCar(CarEntity car) {
		this.car = car;
	}

}
