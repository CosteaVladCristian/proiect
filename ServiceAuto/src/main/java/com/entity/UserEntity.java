package com.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "UserEntity.getAllUser", query = "SELECT c FROM UserEntity c "
				+ "WHERE c.service.id=:serviceId"),
		@NamedQuery(name = "UserEntity.login", query = "SELECT c FROM UserEntity c WHERE"
				+ " c.email=:email and c.password=:password") })
@Entity
@Table(name = "USER_TABLE")
public class UserEntity {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private long id;

	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "EMAIL")
	private String email;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "REGISTER_DATA")
	private long registerData;

	@ManyToOne
	@JoinColumn(name = "SERVICE_ID")
	private ServiceEntity service;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<CarEntity> carList = new ArrayList<CarEntity>();

	
	@OneToMany(mappedBy = "user",cascade=CascadeType.ALL)
	private List<MessageEntity> messageList = new ArrayList<MessageEntity>();

	
	public UserEntity() {

	}

	public UserEntity(String firstName, String email, String lastName, String address, String password, String phone,
			long registerDate) {
		super();
		this.email=email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.password = password;
		this.phone = phone;
		this.registerData = registerDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getRegisterData() {
		return registerData;
	}

	public void setRegisterData(long registerData) {
		this.registerData = registerData;
	}

	public ServiceEntity getService() {
		return service;
	}

	public void setService(ServiceEntity service) {
		this.service = service;
	}

	public List<CarEntity> getCarList() {
		return carList;
	}

	public void setCarList(List<CarEntity> carList) {
		this.carList = carList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getRegisterDate() {
		return registerData;
	}

	public void setRegisterDate(long registerDate) {
		this.registerData = registerDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<MessageEntity> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<MessageEntity> messageList) {
		this.messageList = messageList;
	}

}
