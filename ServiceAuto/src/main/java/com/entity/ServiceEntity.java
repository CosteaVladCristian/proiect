package com.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SERVICE_TABLE")
public class ServiceEntity {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private long id;

	@Column(name = "SERVICE_NAME")
	private String serviceName;

	@OneToMany(mappedBy = "service")
	private List<UserEntity> userList = new ArrayList<UserEntity>();

	public ServiceEntity() {

	}

	public List<UserEntity> getUserList() {
		return userList;
	}

	public void setUserList(List<UserEntity> userList) {
		this.userList = userList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
