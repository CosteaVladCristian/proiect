package com.user.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.CarEntity;
import com.entity.DefectionEntity;
import com.entity.MessageEntity;
import com.entity.ServiceEntity;
import com.entity.UserEntity;
import com.user.dao.ICarEntityDao;
import com.user.dao.IDefectionEntityDao;
import com.user.dao.IMessageEntityDao;
import com.user.dao.IServiceEntityDao;
import com.user.dao.IUserEntityDao;
import com.user.endpoint.model.UserModel;

@Service
@Transactional
public class ControllerUser implements IControllerUser {

	@Autowired
	private IUserEntityDao userEntityDao;
	@Autowired
	private IServiceEntityDao serviceEntityDao;
	@Autowired
	private ICarEntityDao carEntityDao;
	@Autowired
	private IDefectionEntityDao defectionEntityDao;
	@Autowired
	private IMessageEntityDao messageEntityDao;

	@Override
	public UserEntity loginUser(String email, String password) {
		UserEntity entity = userEntityDao.login(email, password);
		if (entity == null) {
			entity = new UserEntity();
			entity.setId(-1l);
		}
		return entity;

	}

	@Override
	public UserEntity registerUser(UserModel userModel) {

		List<ServiceEntity> serviceList = serviceEntityDao.findAll();
		long registerDate = System.currentTimeMillis();
		UserEntity userEntity = new UserEntity(userModel.getFirstName(), userModel.getEmail(), userModel.getLastName(),
				userModel.getAddress(), userModel.getPassword(), userModel.getNumber(), registerDate);
		userEntity.setService(serviceList.get(0));

		CarEntity carEntity = new CarEntity(userModel.getModel(), userModel.getBrand(), userModel.getManufactureYear());
		carEntity.setUser(userEntity);

		DefectionEntity defectionEntity = new DefectionEntity(userModel.getDescription(),
				userModel.getAppointmentData());
		defectionEntity.setCar(carEntity);

		UserEntity entity = userEntityDao.saveAndFlush(userEntity);
		carEntityDao.saveAndFlush(carEntity);
		defectionEntityDao.saveAndFlush(defectionEntity);
		stroreMessage(userModel.getDescription(), userEntity, carEntity);
		return entity;
	}

	private void stroreMessage(String defection, UserEntity user, CarEntity carEntity) {
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setMessage(defection);
		messageEntity.setTitle(defection);
		messageEntity.setUser(user);
		messageEntity.setMessageTs(System.currentTimeMillis());
		messageEntity.setRead(false);
		messageEntity.setSender("user");
		messageEntity.setCar(carEntity);
		messageEntityDao.saveAndFlush(messageEntity);
	}

	@Override
	public UserEntity getUser(long id) {
		return userEntityDao.getOne(id);
	}

}
