package com.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.entity.DefectionEntity;
import com.entity.MessageEntity;
import com.entity.UserEntity;
import com.user.dao.ICarEntityDao;
import com.user.dao.IDefectionEntityDao;
import com.user.dao.IMessageEntityDao;
import com.user.dao.IUserEntityDao;
import com.user.endpoint.model.CarModel;

@Component
public class ControllerCar implements IControllerCar {

	@Autowired
	private IUserEntityDao userEntityDao;
	@Autowired
	private ICarEntityDao carEntityDao;
	@Autowired
	private IDefectionEntityDao defectionEntityDao;
	@Autowired
	private IMessageEntityDao messageEntityDao;

	@Override
	public List<CarEntity> getAllCarsForUser(long userId) {
		return carEntityDao.getAllCarsForUser(userId);
	}

	@Override
	public CarEntity registerCar(CarModel carModel, long userId) {
		CarEntity carEntity = new CarEntity(carModel.getModel(), carModel.getBrand(), carModel.getManufactureYear());
		DefectionEntity defectionEntity = new DefectionEntity(carModel.getDefection(), carModel.getAppointmentDate());
		UserEntity userEntity = userEntityDao.findOne(userId);
		carEntity.setUser(userEntity);
		defectionEntity.setCar(carEntity);
		carEntity = carEntityDao.saveAndFlush(carEntity);
		defectionEntityDao.saveAndFlush(defectionEntity);
		stroreMessage(carModel.getDefection(), userEntity,carEntity);
		return carEntity;

	}

	private void stroreMessage(String defection, UserEntity user, CarEntity carEntity) {
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setMessage(defection);
		messageEntity.setTitle(defection);
		messageEntity.setUser(user);
		messageEntity.setMessageTs(System.currentTimeMillis());
		messageEntity.setRead(false);
		messageEntity.setSender("user");
		messageEntity.setCar(carEntity);
		messageEntityDao.saveAndFlush(messageEntity);
	}

	@Override
	public CarEntity getCarById(long carId) {
		return carEntityDao.getOne(carId);
	}

}
