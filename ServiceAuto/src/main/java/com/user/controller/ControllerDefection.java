package com.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.entity.DefectionEntity;
import com.user.dao.ICarEntityDao;
import com.user.dao.IDefectionEntityDao;
import com.user.endpoint.model.DefectionModel;



@Component
public class ControllerDefection implements IControllerDefection {

	@Autowired
	private ICarEntityDao carEntityDao;
	@Autowired
	private IDefectionEntityDao defectionEntityDao;


	@Override
	public void addDefectionForCar(DefectionModel defectionModel,long carId) {
	DefectionEntity defectionEntity=new DefectionEntity(defectionModel.getDescription(), defectionModel.getAppointmentData());
	CarEntity carEntity=carEntityDao.findOne(carId);
	defectionEntity.setCar(carEntity);
	defectionEntityDao.saveAndFlush(defectionEntity);
	}

	@Override
	public List<DefectionEntity> getDefectionForCar(long carId) {
	CarEntity car = carEntityDao.getOne(carId);
	return car.getDefection();
	
	}

}
