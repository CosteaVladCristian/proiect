package com.user.controller;

import java.util.List;

import com.entity.MessageEntity;
import com.user.endpoint.model.MessageModel;

public interface IMessageController {

	public List<MessageEntity> getAllMessages(long userId);

	List<MessageEntity> getAllUnreadMessages(String sender, long userId);

	public List<MessageEntity> getAllUnreadMessages(long userId, long carId);

	public MessageEntity readMessage(long messageId);

	public void sendMessage(long userId, long carId, MessageModel model);
}
