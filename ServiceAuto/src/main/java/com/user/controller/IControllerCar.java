package com.user.controller;

import java.util.List;

import com.entity.CarEntity;
import com.user.endpoint.model.CarModel;

public interface IControllerCar {
	public List<CarEntity> getAllCarsForUser(long userId);

	public CarEntity registerCar(CarModel carModel, long userId);

	public CarEntity getCarById(long carId);

}
