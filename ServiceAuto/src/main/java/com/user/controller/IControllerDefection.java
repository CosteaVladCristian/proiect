package com.user.controller;

import java.util.List;

import com.entity.DefectionEntity;
import com.user.endpoint.model.DefectionModel;

public interface IControllerDefection {

	public List<DefectionEntity> getDefectionForCar(long carId);

	public void addDefectionForCar(DefectionModel defectionModel, long carId);
}
