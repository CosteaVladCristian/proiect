package com.user.controller;

import com.entity.UserEntity;
import com.user.endpoint.model.UserModel;

public interface IControllerUser {

	UserEntity loginUser(String email, String password);

	UserEntity registerUser(UserModel userModel);

	UserEntity getUser(long id);

}
