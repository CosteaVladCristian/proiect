package com.user.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.MessageEntity;
import com.user.dao.ICarEntityDao;
import com.user.dao.IMessageEntityDao;
import com.user.dao.IUserEntityDao;
import com.user.endpoint.model.MessageModel;

@Service
@Transactional
public class MessageController implements IMessageController {

	@Autowired
	private IMessageEntityDao messageEntityDao;
	@Autowired
	private ICarEntityDao carEntityDao;
	@Autowired
	private IUserEntityDao userEntityDao;

	@Override
	public List<MessageEntity> getAllMessages(long userId) {
		return messageEntityDao.findbyUserId(userId);
	}

	@Override
	public List<MessageEntity> getAllUnreadMessages(String sender,long userId) {
		return messageEntityDao.getAllUnreadMessages(sender,userId);
	}

	@Override
	public List<MessageEntity> getAllUnreadMessages(long userId, long carId) {
		return null;
	}

	@Override
	public MessageEntity readMessage(long messageId) {
		MessageEntity entity = messageEntityDao.getOne(messageId);
		entity.setRead(true);
		messageEntityDao.save(entity);
		return entity;
	}

	@Override
	public void sendMessage(long userId, long carId,MessageModel model) {
		MessageEntity entity = new MessageEntity();
		entity.setCar(carEntityDao.findOne(carId));
		entity.setUser(userEntityDao.getOne(userId));
		entity.setRead(false);
		entity.setMessageTs(System.currentTimeMillis());
		entity.setSender("user");
		
		entity.setMessage(model.getMessage());
		entity.setTitle(model.getTitle());

		messageEntityDao.saveAndFlush(entity);

	}

}
