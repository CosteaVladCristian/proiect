package com.user.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.DefectionEntity;
import com.user.endpoint.model.DefectionModel;
import com.user.service.ServiceDefection;

@Controller
@ComponentScan
@RequestMapping(value = "/userapp/car/defection")
public class CarsDefectionEndpoint {
	@Autowired
	private ServiceDefection serviceDefection;

	@RequestMapping(value = "/all/{userId}/{carId}", method = RequestMethod.GET)
	public String getDefectionForCar(Model model, @PathVariable(value = "carId") long carId,
			@PathVariable(value = "userId") long userId) {
		model.addAttribute("userId", userId);
		model.addAttribute("defection", new DefectionEntity());
		model.addAttribute("defectionCar", serviceDefection.getAllDefectionForCar(carId));
		return "user_templates/userapp_defectionForCar";
	}

	@RequestMapping(value = "/add/{userId}/{carId}", method = RequestMethod.GET)
	public String redirectTo(Model model, @PathVariable(value = "carId") long carId,
			@PathVariable(value = "userId") long userId) {
		model.addAttribute("userId", userId);
		model.addAttribute("carId", carId);
		model.addAttribute("defectionadd", new DefectionEntity());
		return "user_templates/user_app_defection_register";
	}

	@RequestMapping(value = "/add/{userId}/{carId}", method = RequestMethod.POST)
	public String defectionRegister(@ModelAttribute("defectionadd") DefectionModel defectionModel, Model model,
			@PathVariable(value = "userId") long userId, @PathVariable(value = "carId") long carId,
			final RedirectAttributes redirectAttributes) {
		model.addAttribute("userId", userId);
		serviceDefection.defectionRegister(defectionModel, carId);
		model.addAttribute("defection", new DefectionEntity());
		return "redirect:/userapp/car/defection/all/"+userId +"/" +carId;

	}

}
