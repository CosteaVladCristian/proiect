package com.user.endpoint.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DefectionModel {

	@JsonProperty(value = "description")
	private String description;

	@JsonProperty(value = "appointmentData")
	private long appointmentData;


	@JsonCreator
	public DefectionModel() {

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getAppointmentData() {
		return appointmentData;
	}

	public void setAppointmentData(long appointmentData) {
		this.appointmentData = appointmentData;
	}


}
