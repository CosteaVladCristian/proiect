package com.user.endpoint.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginUserModel {
	@JsonProperty(value = "email")
	private String email;
	@JsonProperty(value = "password")
	private String password;

	public LoginUserModel() {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
