package com.user.endpoint.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CarModel {

	@JsonProperty(value = "model")
	private String model;

	@JsonProperty(value = "brand")
	private String brand;

	@JsonProperty(value = "manufactureYear")
	private long manufactureYear;
	
	@JsonProperty(value = "defection")
	private String defection;

	@JsonProperty(value = "appointmentDate")
	private long appointmentDate;


	@JsonProperty(value = "id")
	private long id;

	@JsonCreator
	public CarModel() {

	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public long getManufactureYear() {
		return manufactureYear;
	}

	public void setManufactureYear(long manufactureYear) {
		this.manufactureYear = manufactureYear;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDefection() {
		return defection;
	}

	public void setDefection(String defection) {
		this.defection = defection;
	}

	public long getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(long appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

}