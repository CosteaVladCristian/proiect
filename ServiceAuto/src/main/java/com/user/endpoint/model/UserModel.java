package com.user.endpoint.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserModel {

	@JsonProperty(value = "firstName")
	private String firstName;

	@JsonProperty(value = "lastName")
	private String lastName;

	@JsonProperty(value = "address")
	private String address;

	@JsonProperty(value = "number")
	private String number;

	@JsonProperty(value = "password")
	private String password;

	@JsonProperty(value = "model")
	private String model;

	@JsonProperty(value = "brand")
	private String brand;

	@JsonProperty(value = "manufactureYear")
	private long manufactureYear;

	@JsonProperty(value = "description")
	private String description;

	@JsonProperty(value = "email")
	private String email;

	@JsonProperty(value = "appointmentData")
	private long appointmentData;

	@JsonProperty(value = "defection")
	private String defection;


	@JsonCreator
	public UserModel() {

	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getManufactureYear() {
		return manufactureYear;
	}

	public void setManufactureYear(long manufactureYear) {
		this.manufactureYear = manufactureYear;
	}

	public long getAppointmentData() {
		return appointmentData;
	}

	public void setAppointmentData(long appointmentData) {
		this.appointmentData = appointmentData;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDefection() {
		return defection;
	}

	public void setDefection(String defection) {
		this.defection = defection;
	}

}
