package com.user.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.UserEntity;
import com.user.endpoint.model.MessageModel;
import com.user.service.MessageService;



@Controller
@ComponentScan
@RequestMapping(value = "/userapp/message")
public class MessageServiceEndpoint {

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public String sendMessageTemplate(Model model, @PathVariable(value = "userId") long userId) {
		model.addAttribute("messageModel", new MessageModel());
		model.addAttribute("userId", userId);
		return "user_templates/user_app_message_template";
	}
	
	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/all/{userId}", method = RequestMethod.GET)
	public String getAllMessages(Model model, @PathVariable(value = "userId") long userId) {
		model.addAttribute("messageList", new MessageModel());
		model.addAttribute("messageList", messageService.getAllMessages(userId));
		return "user_templates/serviceauto_messages";
	}

	@RequestMapping(value = "/unread/{userId}", method = RequestMethod.GET)
	public String getAllUnreadMessage(Model model, @PathVariable(value = "userId") long userId) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new MessageModel());
		model.addAttribute("messageList", messageService.getAllUnreadMessages("service",userId));
		return "user_templates/serviceauto_messages";
	}

	@RequestMapping(value = "/read/{messageId}", method = RequestMethod.GET)
	public String readMesage(Model model, @PathVariable(value = "messageId") long messageId) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new MessageModel());
		model.addAttribute("message", messageService.readMessage(messageId));
		return "user_templates/serviceauto_message_content";
	}

	@RequestMapping(value = "/send/{userId}/{carId}", method = RequestMethod.GET)
	public String redirectToSendMessageToUser(Model model, @PathVariable(value = "userId") long userId,
			@PathVariable(value = "carId") long carId) {
		MessageModel msgModel = new MessageModel();
		msgModel.setUserId(userId);
		msgModel.setCarId(carId);
		model.addAttribute("userId", userId);
		model.addAttribute("carId", userId);
		model.addAttribute("message", msgModel);
		return "user_templates/serviceauto_send_messages";
	}

	@RequestMapping(value = "/send/{userId}/{carId}", method = RequestMethod.POST)
	public String sendMessageToUser(@ModelAttribute("message") MessageModel messageModel, Model model,
			@PathVariable(value = "userId") long userId, @PathVariable(value = "carId") long carId) {
		messageService.sendMessage(userId, carId, messageModel);
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new MessageModel());
		model.addAttribute("messageList", messageService.getAllUnreadMessages("user",userId));
		return "user_templates/serviceauto_messages";
	}
	
}
