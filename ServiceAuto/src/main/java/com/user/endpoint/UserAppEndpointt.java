package com.user.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.UserEntity;
import com.user.endpoint.model.LoginUserModel;
import com.user.endpoint.model.UserModel;
import com.user.service.ServiceUser;

@Controller
@ComponentScan
@RequestMapping(value = "/userapp")
public class UserAppEndpointt {

	@Autowired
	private ServiceUser serviceUser;

	@RequestMapping(method = RequestMethod.GET)
	public String redirectToMainPage(Model model) {
		model.addAttribute("loginUser", new LoginUserModel());
		return "user_templates/user_app_login_page";
	}

	@RequestMapping(value = "/home/{userId}", method = RequestMethod.GET)
	public String redirectToHomePage(@PathVariable(value = "userId") long id, Model model) {
		model.addAttribute("userEntity", serviceUser.getUser(id));
		return "user_templates/user_app_main_loggedin";
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String redirectToContactPage() {
		return "user_templates/user_app_contact_page";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		model.addAttribute("loginUser", new LoginUserModel());
		return "user_templates/user_app_login_page";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String redirectToRegisterPage(Model model) {
		model.addAttribute("userRegisterModel", new UserModel());
		return "user_templates/user_app_register_page";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(Model model, @ModelAttribute("userRegisterModel") UserModel userModel) {
		UserEntity userEntity = serviceUser.registerUser(userModel);
		model.addAttribute("userRegisterModel", new UserModel());
		model.addAttribute("userEntity", userEntity);
		return "user_templates/user_app_main_loggedin";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@ModelAttribute("loginUser") LoginUserModel loginUserModel, Model model) {
		model.addAttribute("userEntity", new UserEntity());
		UserEntity userEntity = serviceUser.loginUser(loginUserModel);
		if (userEntity.getId() > 0) {
			model.addAttribute("userEntity", userEntity);
			return "user_templates/user_app_main_loggedin";
		}
		return "user_templates/user_app_error_login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String redirectToLoginPage(Model model) {
		model.addAttribute("loginUser", new LoginUserModel());
		return "user_templates/user_app_login_page";
	}
}
