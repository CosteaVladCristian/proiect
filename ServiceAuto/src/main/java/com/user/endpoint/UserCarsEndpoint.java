package com.user.endpoint;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.CarEntity;
import com.user.endpoint.model.CarModel;
import com.user.service.ServiceCar;
import com.user.service.ServiceUser;

@Controller
@ComponentScan
@RequestMapping(value = "/userapp/car")
public class UserCarsEndpoint {

	@Autowired
	private ServiceCar serviceCar;
	@Autowired
	private ServiceUser serviceUser;

	@RequestMapping(value = "/all/{userId}", method = RequestMethod.GET)
	public String getAllCarsForUser(Model model, @PathVariable(value = "userId") long userId) {
		model.addAttribute("userId", userId);
		model.addAttribute("car", new CarEntity());
		model.addAttribute("userCarList", (ArrayList<CarEntity>) serviceCar.getAllCarsForUser(userId));
		return "user_templates/user_app_cars";
	}

	@RequestMapping(value = "/register/{userId}", method = RequestMethod.GET)
	public String redirectToUser(Model model, @PathVariable(value = "userId") long userId) {
		model.addAttribute("userId", userId);
		model.addAttribute("caradd", new CarModel());
		return "user_templates/user_app_car_register";
	}

	@RequestMapping(value = "/message/{userId}/{carId}", method = RequestMethod.GET)
	public String addCarResponseMessage(Model model, @PathVariable(value = "userId") long userId,
			@PathVariable(value = "carId") long carId, final RedirectAttributes redirectAttributes) {
		model.addAttribute("userId", userId);
		model.addAttribute("carEntity", serviceCar.getCarById(carId));
		model.addAttribute("userEntity", serviceUser.getUser(userId));
		return "user_templates/user_app_car_register_message";
	}

	@RequestMapping(value = "/register/{userId}", method = RequestMethod.POST)
	public String carRegister(@ModelAttribute("caradd") CarModel carModel, Model model,
			@PathVariable(value = "userId") long userId) {
		CarEntity carEntity = serviceCar.registerCar(carModel, userId);
		model.addAttribute("car", new CarEntity());
		model.addAttribute("userCarList", (ArrayList<CarEntity>) serviceCar.getAllCarsForUser(userId));
		return "redirect:/userapp/car/message/" + userId + "/" + carEntity.getId();
	}
}
