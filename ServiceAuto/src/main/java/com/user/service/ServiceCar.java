package com.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.user.controller.IControllerCar;
import com.user.endpoint.model.CarModel;

@Component
public class ServiceCar {
	@Autowired
	private IControllerCar controllerCar;

	public List<CarEntity> getAllCarsForUser(long id) {
		List<CarEntity> carList = new ArrayList<CarEntity>();
		try {
			carList = controllerCar.getAllCarsForUser(id);
		} catch (IllegalArgumentException e) {
		}

		return carList;
	}
	public CarEntity registerCar(CarModel carModel, long userId) {
		return controllerCar.registerCar(carModel, userId);
	}

	public CarEntity getCarById( long carId) {
		return controllerCar.getCarById(carId);
	}
}
