package com.user.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.DefectionEntity;
import com.user.controller.IControllerDefection;
import com.user.endpoint.model.DefectionModel;

@Component
public class ServiceDefection {
	@Autowired
	private IControllerDefection controllerDefection;

	public List<DefectionEntity> getAllDefectionForCar(long carId) {
		List<DefectionEntity> defectionEntity = new ArrayList<DefectionEntity>();
		defectionEntity = controllerDefection.getDefectionForCar(carId);
		return defectionEntity;
	}

	public void defectionRegister(DefectionModel defectionModel, long carId) {
		controllerDefection.addDefectionForCar(defectionModel,carId);
	}
}
