package com.user.service;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.UserEntity;
import com.user.controller.IControllerUser;
import com.user.endpoint.model.UserModel;

@Service
@Transactional
public class ServiceUser {

	@Autowired
	private IControllerUser userController;

	public UserEntity loginUser(com.user.endpoint.model.LoginUserModel loginUserModel) {
		return userController.loginUser(loginUserModel.getEmail(), loginUserModel.getPassword());
	}

	public UserEntity registerUser(UserModel userModel) {
		return userController.registerUser(userModel);
		
	}
	public UserEntity getUser(long id) {
		return userController.getUser(id);
		
	}

}
