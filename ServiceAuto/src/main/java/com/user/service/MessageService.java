package com.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.MessageEntity;
import com.user.controller.IMessageController;
import com.user.endpoint.model.MessageModel;


@Component
public class MessageService {
	@Autowired
	private IMessageController messageController;

	public List<MessageModel> getAllMessages(long userId) {
		List<MessageEntity> messageEntityList = messageController.getAllMessages(userId);
		List<MessageModel> saMessageResponseModel = new ArrayList<MessageModel>();
		for (MessageEntity m : messageEntityList) {
			saMessageResponseModel.add(buildMessageModel(m));
		}
		return saMessageResponseModel;
	}

	public MessageModel readMessage(long messageId) {
		MessageEntity msg = messageController.readMessage(messageId);
		MessageModel model = new MessageModel();
		model.setEmail(msg.getUser().getEmail());
		model.setTitle(msg.getTitle());
		model.setMessage(msg.getMessage());
		model.setPhone(msg.getUser().getPhone());
		model.setName(msg.getUser().getFirstName() + " " + msg.getUser().getLastName());
		model.setUserId(msg.getUser().getId());
		model.setCarId(msg.getCar().getId());
		model.setMessageId(msg.getId());
		return model;
	}

	public List<MessageModel> getAllUnreadMessages(String sender, long userId) {
		List<MessageEntity> messageEntityList = messageController.getAllUnreadMessages(sender,userId);
		List<MessageModel> saMessageResponseModel = new ArrayList<MessageModel>();
		for (MessageEntity m : messageEntityList) {
			MessageModel model = new MessageModel();
			model.setEmail(m.getUser().getEmail());
			model.setTitle(m.getTitle().substring(0,m.getTitle().length()/2) + "...");
			model.setPhone(m.getUser().getPhone());
			model.setName(m.getUser().getFirstName() + " " + m.getUser().getLastName());
			model.setUserId(m.getUser().getId());
			model.setCarId(m.getCar().getId());
			model.setMessageId(m.getId());
			saMessageResponseModel.add(model);
		}
		return saMessageResponseModel;

	}

	private MessageModel buildMessageModel(MessageEntity m) {
		MessageModel model = new MessageModel();
		model.setMessage(m.getMessage());
		model.setMsgTs(m.getMessageTs());
		model.setName(m.getUser().getFirstName() + " " + m.getUser().getLastName());
		model.setEmail(m.getUser().getEmail());
		model.setPhone(m.getUser().getPhone());
		model.setTitle(m.getTitle());
		model.setModel(m.getCar().getModel());
		model.setBrand(m.getCar().getBrand());
		model.setMessageId(m.getId());
		model.setManufactureYear(m.getCar().getManufactureYear());
		return model;
	}

	public void sendMessage(long userId, long carId, MessageModel model) {
		messageController.sendMessage(userId, carId, model);

	}

}
