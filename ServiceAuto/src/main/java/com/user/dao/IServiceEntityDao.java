package com.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.ServiceEntity;


public interface IServiceEntityDao extends JpaRepository<ServiceEntity, Long> {

}
