package com.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.MessageEntity;

public interface IMessageEntityDao extends JpaRepository<MessageEntity, Long> {
	@Query(name = "MessageEntity.getAllUnreadMessages")
	List<MessageEntity> getAllUnreadMessages(@Param("sender") String sender, @Param("userId")long userId);
	
	@Query(name = "MessageEntity.findbyUserId")
	List<MessageEntity> findbyUserId(@Param("userId")long userId);


}
