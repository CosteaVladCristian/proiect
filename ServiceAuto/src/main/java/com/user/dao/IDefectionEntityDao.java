package com.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.DefectionEntity;


public interface IDefectionEntityDao extends JpaRepository<DefectionEntity, Long> {

}
