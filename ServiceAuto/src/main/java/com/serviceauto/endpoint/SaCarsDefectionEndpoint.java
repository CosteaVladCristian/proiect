package com.serviceauto.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.DefectionEntity;
import com.serviceauto.endpoint.model.SaDefectionModel;
import com.serviceauto.service.DefectionService;

@Controller
@ComponentScan
@RequestMapping(value = "/serviceauto/car/defection")
public class SaCarsDefectionEndpoint {
	@Autowired
	private DefectionService defectionService;

	 @RequestMapping(value = "/{carId}", method = RequestMethod.POST)
	 public String addDefection(@RequestBody SaDefectionModel defectionModel,
	 @PathVariable(value = "carId") long carId) {
	 defectionService.addDefection(defectionModel, carId);
	 return "index";
	 }

	@RequestMapping(value = "/all/{carId}", method = RequestMethod.GET)
	public String getDefectionForCar(Model model, @PathVariable(value = "carId") long carId) {
		model.addAttribute("defection", new DefectionEntity());
		model.addAttribute("defectionCar", defectionService.getAllDefectionForCar(carId));
		return "service_templates/defectionForCar";
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String getAllDefection(Model model) {
		model.addAttribute("defection", new DefectionEntity());
		model.addAttribute("allDefection", defectionService.getAllDefection());

		return "service_templates/all_cars_defection";
	}

	@RequestMapping(value = "/{carId}", method = RequestMethod.GET)
	public String redirectTo(Model model, @PathVariable(value = "carId") long carId) {
		model.addAttribute("defectionadd", new DefectionEntity());
		model.addAttribute("userIdCar", carId);
		return "service_templates/defection_register";
	}

	@RequestMapping(value = "/register/{carId}", method = RequestMethod.POST)
	public String defectionRegister(@ModelAttribute("defectionadd") SaDefectionModel defectionModel, Model model,
			@PathVariable(value = "carId") long carId) {
		defectionService.defectionRegister(defectionModel,carId);
		model.addAttribute("carId", carId);
		model.addAttribute("defection", new DefectionEntity());
		model.addAttribute("allDefection", defectionService.getAllDefection());
		return "service_templates/all_cars_defection";

	}

}
