package com.serviceauto.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.UserEntity;
import com.serviceauto.endpoint.model.SaMessageModel;
import com.serviceauto.service.SaMessageService;

@Controller
@ComponentScan
@RequestMapping(value = "/serviceauto/messages")
public class SaMessagesServiceEndpoint {

	@Autowired
	private SaMessageService messageService;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String getAllMessages(Model model) {
		model.addAttribute("messageList", new SaMessageModel());
		model.addAttribute("messageList", messageService.getAllMessages());
		return "service_templates/serviceauto_messages";
	}

	@RequestMapping(value = "/unread", method = RequestMethod.GET)
	public String getAllUnreadMessage(Model model) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new SaMessageModel());
		model.addAttribute("messageList", messageService.getAllUnreadMessages("user"));
		return "service_templates/serviceauto_messages";
	}

	@RequestMapping(value = "/read/{messageId}", method = RequestMethod.GET)
	public String readMesage(Model model, @PathVariable(value = "messageId") long messageId) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new SaMessageModel());
		model.addAttribute("message", messageService.readMessage(messageId));
		return "service_templates/serviceauto_message_content";
	}

	@RequestMapping(value = "/send/{userId}/{carId}", method = RequestMethod.GET)
	public String redirectToSendMessageToUser(Model model, @PathVariable(value = "userId") long userId,
			@PathVariable(value = "carId") long carId) {
		SaMessageModel msgModel = new SaMessageModel();
		msgModel.setUserId(userId);
		msgModel.setCarId(carId);
		model.addAttribute("userId", userId);
		model.addAttribute("carId", userId);
		model.addAttribute("message", msgModel);
		return "service_templates/serviceauto_send_messages";
	}

	@RequestMapping(value = "/send/{userId}/{carId}", method = RequestMethod.POST)
	public String sendMessageToUser(@ModelAttribute("message") SaMessageModel messageModel, Model model,
			@PathVariable(value = "userId") long userId, @PathVariable(value = "carId") long carId) {
		messageService.sendMessage(userId, carId, messageModel);
		model.addAttribute("user", new UserEntity());
		model.addAttribute("message", new SaMessageModel());
		model.addAttribute("messageList", messageService.getAllUnreadMessages("user"));
		return "service_templates/serviceauto_messages";
	}
}
