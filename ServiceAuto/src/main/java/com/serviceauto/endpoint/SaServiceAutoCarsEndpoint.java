package com.serviceauto.endpoint;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.CarEntity;
import com.serviceauto.endpoint.model.SaCarModel;
import com.serviceauto.service.CarService;

@Controller
@ComponentScan
@RequestMapping(value = "/serviceauto/car")
public class SaServiceAutoCarsEndpoint {

	@Autowired
	private CarService carService;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String getAllCars(Model model) {
		model.addAttribute("car", new CarEntity());
		model.addAttribute("allCars", (ArrayList<CarEntity>) carService.getAllCars());
		return "service_templates/all_user_cars";
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public String getAllCarsForUser(Model model, @PathVariable(value = "userId") long id) {
		model.addAttribute("car", new CarEntity());
		model.addAttribute("userCarList", (ArrayList<CarEntity>) carService.getAllCarsForUser(id));
		return "service_templates/user_cars";
	}

	@RequestMapping(value = "/register/car/{userId}", method = RequestMethod.POST)
	public String registerCar(@RequestBody SaCarModel carModel, @PathVariable(value = "userId") long userId) {
		carService.registerCar(carModel, userId);

		return "service_templates/all_user_cars";
	}

	@RequestMapping(value ="/userId/{userId}",method = RequestMethod.GET)
	public String redirectToUser(Model model,@PathVariable(value = "userId") long userId) {
		model.addAttribute("userIdCar", userId);
		model.addAttribute("caradd", new CarEntity());
		return "service_templates/car_register";
	}

	@RequestMapping(value = "/register/{userId}", method = RequestMethod.POST)
	public String carRegister(@ModelAttribute("caradd") SaCarModel carModel, Model model,@PathVariable(value = "userId") long userId) {
		carService.registerCar(carModel,userId);
		model.addAttribute("car", new CarEntity());
		model.addAttribute("allCars", (ArrayList<CarEntity>) carService.getAllCars());
		return "service_templates/all_user_cars";
	}
}
