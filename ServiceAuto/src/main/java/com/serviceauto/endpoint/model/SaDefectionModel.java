package com.serviceauto.endpoint.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SaDefectionModel {

	@JsonProperty(value = "description")
	private String description;

	@JsonProperty(value = "appointmentData")
	private long appointmentData;

	@JsonProperty(value = "id")
	private long id;

	@JsonCreator
	public SaDefectionModel() {

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getAppointmentData() {
		return appointmentData;
	}

	public void setAppointmentData(long appointmentData) {
		this.appointmentData = appointmentData;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
