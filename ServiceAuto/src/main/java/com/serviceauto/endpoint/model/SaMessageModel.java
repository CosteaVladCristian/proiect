package com.serviceauto.endpoint.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SaMessageModel {

	@JsonProperty(value = "name")
	private String name;
	@JsonProperty(value = "message")
	private String message;
	@JsonProperty(value = "msgTs")
	private long msgTs;
	@JsonProperty(value = "email")
	private String email;
	@JsonProperty(value = "phone")
	private String phone;
	@JsonProperty(value = "title")
	private String title;
	@JsonProperty(value = "model")
	private String model;
	@JsonProperty(value = "brand")
	private String brand;
	@JsonProperty(value = "manufactureYear")
	private long manufactureYear;
	@JsonProperty(value = "appointmentDate")
	private String appointmentDate;
	@JsonProperty(value = "userId")
	private long userId;
	@JsonProperty(value = "carId")
	private long carId;
	@JsonProperty(value = "messageId")
	private long messageId;

	public SaMessageModel() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getMsgTs() {
		return msgTs;
	}

	public void setMsgTs(long msgTs) {
		this.msgTs = msgTs;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public long getManufactureYear() {
		return manufactureYear;
	}

	public void setManufactureYear(long manufactureYear) {
		this.manufactureYear = manufactureYear;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCarId() {
		return carId;
	}

	public void setCarId(long carId) {
		this.carId = carId;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
}
