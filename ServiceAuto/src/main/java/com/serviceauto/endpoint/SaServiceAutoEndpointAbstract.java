package com.serviceauto.endpoint;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@ComponentScan
@RequestMapping(value = "/serviceauto")
public class SaServiceAutoEndpointAbstract {

@RequestMapping(method= RequestMethod.GET)
public String redirectToMainPage(){
	return "service_templates/service_auto_main_page";
}


} 
