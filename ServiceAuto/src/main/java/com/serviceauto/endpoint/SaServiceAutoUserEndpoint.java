package com.serviceauto.endpoint;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.entity.UserEntity;
import com.serviceauto.endpoint.model.SaUserModel;
import com.serviceauto.service.UserService;

@Controller
@ComponentScan
@RequestMapping(value = "/serviceauto/user")
public class SaServiceAutoUserEndpoint {
	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET)
	public String redirectToMainPage(Model model) {
		model.addAttribute("useradd", new SaUserModel());
		return "service_templates/service_auto_add_user_page";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("useradd") SaUserModel userModel, Model model,
			final RedirectAttributes redirectAttributes) {
		boolean error = false;
		try {
			userService.registerUser(userModel);
		} catch (Exception e) {

			error = true;

		}
		if (error) {
			redirectAttributes.addFlashAttribute("user", "unsuccess");
			return "redirect:/serviceauto/user";
		} else {
			redirectAttributes.addFlashAttribute("user", "success");
			model.addAttribute("user", new UserEntity());
			model.addAttribute("userList", (ArrayList<UserEntity>) userService.getAllUsers());
			return "redirect:/serviceauto/user/all";
		}

		
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String getAllUser(Model model) {
		model.addAttribute("user", new UserEntity());
		model.addAttribute("userList", (ArrayList<UserEntity>) userService.getAllUsers());
		return "service_templates/all_users";

	}

	public String sendMessageToUser() {
		return "service_templates/serviceauto_";
	}
}
