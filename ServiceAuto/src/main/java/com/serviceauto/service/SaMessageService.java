package com.serviceauto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.MessageEntity;
import com.serviceauto.controller.ISaMessageController;
import com.serviceauto.endpoint.model.SaMessageModel;

@Component
public class SaMessageService {
	@Autowired
	private ISaMessageController messageController;

	public List<SaMessageModel> getAllMessages() {
		List<MessageEntity> messageEntityList = messageController.getAllMessages();
		List<SaMessageModel> saMessageResponseModel = new ArrayList<SaMessageModel>();
		for (MessageEntity m : messageEntityList) {
			saMessageResponseModel.add(buildMessageModel(m));
		}
		return saMessageResponseModel;
	}

	public SaMessageModel readMessage(long messageId) {
		MessageEntity msg = messageController.readMessage(messageId);
		SaMessageModel model = new SaMessageModel();
		model.setEmail(msg.getUser().getEmail());
		model.setTitle(msg.getTitle());
		model.setMessage(msg.getMessage());
		model.setPhone(msg.getUser().getPhone());
		model.setName(msg.getUser().getFirstName() + " " + msg.getUser().getLastName());
		model.setUserId(msg.getUser().getId());
		model.setCarId(msg.getCar().getId());
		model.setMessageId(msg.getId());
		return model;
	}

	public List<SaMessageModel> getAllUnreadMessages(String sender) {
		List<MessageEntity> messageEntityList = messageController.getAllUnreadMessages(sender);
		List<SaMessageModel> saMessageResponseModel = new ArrayList<SaMessageModel>();
		for (MessageEntity m : messageEntityList) {
			SaMessageModel model = new SaMessageModel();
			model.setEmail(m.getUser().getEmail());
			model.setTitle(m.getTitle().substring(0,m.getTitle().length()/2) + "...");
			model.setPhone(m.getUser().getPhone());
			model.setName(m.getUser().getFirstName() + " " + m.getUser().getLastName());
			model.setUserId(m.getUser().getId());
			model.setCarId(m.getCar().getId());
			model.setMessageId(m.getId());
			saMessageResponseModel.add(model);
		}
		return saMessageResponseModel;

	}

	private SaMessageModel buildMessageModel(MessageEntity m) {
		SaMessageModel model = new SaMessageModel();
		model.setMessage(m.getMessage());
		model.setMsgTs(m.getMessageTs());
		model.setName(m.getUser().getFirstName() + " " + m.getUser().getLastName());
		model.setEmail(m.getUser().getEmail());
		model.setPhone(m.getUser().getPhone());
		model.setTitle(m.getTitle());
		model.setModel(m.getCar().getModel());
		model.setBrand(m.getCar().getBrand());
		model.setManufactureYear(m.getCar().getManufactureYear());
		return model;
	}

	public void sendMessage(long userId, long carId, SaMessageModel model) {
		messageController.sendMessage(userId, carId, model);

	}

}
