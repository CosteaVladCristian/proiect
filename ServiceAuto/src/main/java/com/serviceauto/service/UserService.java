package com.serviceauto.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.UserEntity;
import com.serviceauto.controller.ISaUserController;
import com.serviceauto.endpoint.model.SaUserModel;

@Service
@Transactional
public class UserService {
	@Autowired
	private ISaUserController userRegisterController;

	public void registerUser(SaUserModel userModel) {
		userRegisterController.registerUser(userModel);

	}

	public List<UserEntity> getAllUsers() {
		List<UserEntity> userList = new ArrayList<UserEntity>();
		try {
			userList = userRegisterController.getAllUsers();
		} catch (IllegalArgumentException e) {

		}
		return userList;
	}

	public void mainRegisterUser(SaUserModel userModel) {
		userRegisterController.mainRegisterUser(userModel);

	}

}
