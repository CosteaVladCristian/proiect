package com.serviceauto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.serviceauto.controller.ISaCarController;
import com.serviceauto.endpoint.model.SaCarModel;


@Component
public class CarService {
	@Autowired
	private ISaCarController carController;

	public List<CarEntity> getAllCars() {
		List<CarEntity> carList = new ArrayList<CarEntity>();
		try {
			carList = carController.getAllCarsFromService();
		} catch (IllegalArgumentException e) {

		}

		return carList;
	}

	public List<CarEntity> getAllCarsForUser(long id) {
		List<CarEntity> carList = new ArrayList<CarEntity>();
		try {
			carList = carController.getAllCarsForUser(id);
		} catch (IllegalArgumentException e) {
		}

		return carList;
	}

	public void registerCar(SaCarModel carModel, long userId) {
		carController.addRegisterCar(carModel, userId);
	}

	public void carRegister(SaCarModel carModel) {
		carController.carRegister(carModel);

	}

}
