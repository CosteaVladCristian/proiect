package com.serviceauto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.DefectionEntity;
import com.serviceauto.controller.ISaDefectionController;
import com.serviceauto.endpoint.model.SaDefectionModel;


@Component
public class DefectionService {
	@Autowired
	private ISaDefectionController defectionController;

	public List<DefectionEntity> getAllDefectionForCar(long carId) {
		List<DefectionEntity> defectionEntity = new ArrayList<DefectionEntity>();
		defectionEntity = defectionController.getDefectionForCar(carId);
		return defectionEntity;
	}

	public List<DefectionEntity> getAllDefection() {
		List<DefectionEntity> defectionList = new ArrayList<DefectionEntity>();
		defectionList = defectionController.getAllDefectionFromService();
		return defectionList;
	}

	public void addDefection(SaDefectionModel defectionModel, long carId) {
		defectionController.addDefection(defectionModel, carId);

	}

	public void defectionRegister(SaDefectionModel defectionModel, long carId) {

		defectionController.defectionRegister(defectionModel,carId);
	}
}
