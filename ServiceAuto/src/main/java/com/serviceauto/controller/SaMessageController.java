package com.serviceauto.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.MessageEntity;
import com.serviceauto.dao.ISaCarEntityDao;
import com.serviceauto.dao.ISaMessageEntityDao;
import com.serviceauto.dao.ISaUserEntityDao;
import com.serviceauto.endpoint.model.SaMessageModel;

@Service
@Transactional
public class SaMessageController implements ISaMessageController {

	@Autowired
	private ISaMessageEntityDao saMessageEntityDao;
	@Autowired
	private ISaCarEntityDao saCarEntityDao;
	@Autowired
	private ISaUserEntityDao saUserEntityDao;

	@Override
	public List<MessageEntity> getAllMessages() {
		return saMessageEntityDao.findAll();
	}

	@Override
	public List<MessageEntity> getAllUnreadMessages(String sender) {
		return saMessageEntityDao.getSaAllUnreadMessages(sender);
	}



	@Override
	public MessageEntity readMessage(long messageId) {
		MessageEntity entity = saMessageEntityDao.getOne(messageId);
		entity.setRead(true);
		saMessageEntityDao.save(entity);
		return entity;
	}

	@Override
	public void sendMessage(long userId, long carId, SaMessageModel model) {
		MessageEntity entity = new MessageEntity();
		entity.setCar(saCarEntityDao.findOne(carId));
		entity.setUser(saUserEntityDao.getOne(userId));
		entity.setRead(false);
		entity.setMessageTs(System.currentTimeMillis());
		entity.setSender("service");
		
		entity.setMessage(model.getMessage());
		entity.setTitle(model.getTitle());

		saMessageEntityDao.saveAndFlush(entity);

	}

}
