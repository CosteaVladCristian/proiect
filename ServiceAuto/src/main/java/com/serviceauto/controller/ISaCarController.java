package com.serviceauto.controller;

import java.util.List;

import com.entity.CarEntity;
import com.serviceauto.endpoint.model.SaCarModel;



public interface ISaCarController {
	public List<CarEntity> getAllCarsFromService();

	public List<CarEntity> getAllCarsForUser(long userId);


	public void addRegisterCar(SaCarModel carModel, long userId);

	public void carRegister(SaCarModel carModel);

 }
