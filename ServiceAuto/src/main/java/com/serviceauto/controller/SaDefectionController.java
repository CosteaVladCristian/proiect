package com.serviceauto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.entity.DefectionEntity;
import com.serviceauto.dao.ISaCarEntityDao;
import com.serviceauto.dao.ISaDefectionEntityDao;
import com.serviceauto.endpoint.model.SaDefectionModel;


@Component
public class SaDefectionController implements ISaDefectionController {
	@Autowired
	private ISaDefectionEntityDao defectionEntityDao;
	@Autowired
	private ISaCarEntityDao carEntityDao;

	@Override
	public List<DefectionEntity> getAllDefectionFromService() {
		return defectionEntityDao.findAll();
	}

	@Override
	public void addDefection(SaDefectionModel defectionModel, long carId) {
		DefectionEntity defectionEntity = new DefectionEntity(defectionModel.getDescription(),
				defectionModel.getAppointmentData());
		CarEntity carEntity = carEntityDao.findOne(carId);
		defectionEntity.setCar(carEntity);
		defectionEntityDao.saveAndFlush(defectionEntity);
	}

	@Override
	public List<DefectionEntity> getDefectionForCar(long carId) {
	CarEntity car = carEntityDao.getOne(carId);
	return car.getDefection();
	
	}

	@Override
	public void defectionRegister(SaDefectionModel defectionModel,long carId) {
	DefectionEntity defectionEntity=new DefectionEntity(defectionModel.getDescription(), defectionModel.getAppointmentData());
	CarEntity carEntity=carEntityDao.findOne(carId);
	defectionEntity.setCar(carEntity);
	defectionEntityDao.saveAndFlush(defectionEntity);
	}

}
