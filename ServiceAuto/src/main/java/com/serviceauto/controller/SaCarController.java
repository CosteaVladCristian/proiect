package com.serviceauto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entity.CarEntity;
import com.entity.UserEntity;
import com.serviceauto.dao.ISaCarEntityDao;
import com.serviceauto.dao.ISaUserEntityDao;
import com.serviceauto.endpoint.model.SaCarModel;


@Component
public class SaCarController implements ISaCarController {
	@Autowired
	private ISaCarEntityDao carEntityDao;
	@Autowired
	ISaUserEntityDao userEntityDao;

	@Override
	public List<CarEntity> getAllCarsFromService() {
		return carEntityDao.findAll();
	}

	@Override
	public List<CarEntity> getAllCarsForUser(long userId) {
		return carEntityDao.getAllCarsForUser(userId);
	}

	@Override
	public void addRegisterCar(SaCarModel carModel, long userId) {
		CarEntity carEntity = new CarEntity(carModel.getModel(), carModel.getBrand(), carModel.getManufactureYear());
		UserEntity userEntity = userEntityDao.findOne(userId);
		carEntity.setUser(userEntity);
		carEntityDao.saveAndFlush(carEntity);

	}

	@Override
	public void carRegister(SaCarModel carModel) {
		CarEntity carEntity = new CarEntity(carModel.getModel(), carModel.getBrand(), carModel.getManufactureYear());
		UserEntity userEntity = userEntityDao.findOne(carModel.getId());
		carEntity.setUser(userEntity);
		carEntityDao.saveAndFlush(carEntity);

	}

}
