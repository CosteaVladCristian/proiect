package com.serviceauto.controller;

import java.util.List;

import com.entity.DefectionEntity;
import com.serviceauto.endpoint.model.SaDefectionModel;


public interface ISaDefectionController {

	public List<DefectionEntity> getAllDefectionFromService();

	public void addDefection(SaDefectionModel defectionModel, long carId);

	public List<DefectionEntity> getDefectionForCar(long carId);

	public void defectionRegister(SaDefectionModel defectionModel, long carId);

}
