package com.serviceauto.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.CarEntity;
import com.entity.DefectionEntity;
import com.entity.ServiceEntity;
import com.entity.UserEntity;
import com.serviceauto.dao.ISaCarEntityDao;
import com.serviceauto.dao.ISaDefectionEntityDao;
import com.serviceauto.dao.ISaServiceEntityDao;
import com.serviceauto.dao.ISaUserEntityDao;
import com.serviceauto.endpoint.model.SaUserModel;


@Service
@Transactional
public class SaUserController implements ISaUserController {
	@Autowired
	private ISaUserEntityDao userEntityDao;

	@Autowired
	private ISaServiceEntityDao serviceEntityDao;

	@Autowired
	private ISaDefectionEntityDao defectionEntityDao;

	@Autowired
	private ISaCarEntityDao carEntityDao;

	public void registerUser(SaUserModel userModel) {

		List<ServiceEntity> serviceList = serviceEntityDao.findAll();
		long registerDate = System.currentTimeMillis();
		UserEntity userEntity = new UserEntity(userModel.getFirstName(), userModel.getEmail(), userModel.getLastName(),
				userModel.getAdress(), userModel.getPassword(), userModel.getNumber(), registerDate);
		userEntity.setService(serviceList.get(0));

		CarEntity carEntity = new CarEntity(userModel.getModel(), userModel.getBrand(), userModel.getManufactureYear());
		carEntity.setUser(userEntity);

		DefectionEntity defectionEntity = new DefectionEntity(userModel.getDescription(),
				userModel.getAppointmentData());
		defectionEntity.setCar(carEntity);

		userEntityDao.saveAndFlush(userEntity);
		carEntityDao.saveAndFlush(carEntity);
		defectionEntityDao.saveAndFlush(defectionEntity);
	}

	public void mainRegisterUser(SaUserModel userModel) {
		long registerDate = System.currentTimeMillis();
		List<ServiceEntity> serviceList = serviceEntityDao.findAll();
		UserEntity userEntity = new UserEntity(userModel.getFirstName(), userModel.getEmail(), userModel.getLastName(),
				userModel.getAdress(), userModel.getPassword(), userModel.getNumber(), registerDate);
		userEntity.setService(serviceList.get(0));
		userEntityDao.saveAndFlush(userEntity);

	}

	@Override
	public List<UserEntity> getAllUsers() {
		return userEntityDao.findAll();
	}

}
