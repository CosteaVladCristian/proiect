package com.serviceauto.controller;

import java.util.List;

import com.entity.MessageEntity;
import com.serviceauto.endpoint.model.SaMessageModel;

public interface ISaMessageController {

	public List<MessageEntity> getAllMessages();

	List<MessageEntity> getAllUnreadMessages(String sender);

	public MessageEntity readMessage(long messageId);

	public void sendMessage(long userId, long carId, SaMessageModel model);
}
