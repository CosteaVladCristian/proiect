package com.serviceauto.controller;

import java.util.List;

import com.entity.UserEntity;
import com.serviceauto.endpoint.model.SaUserModel;


public interface ISaUserController {

	public void registerUser(SaUserModel userModel);

	public List<UserEntity> getAllUsers();

	public void mainRegisterUser(SaUserModel userModel);

}
