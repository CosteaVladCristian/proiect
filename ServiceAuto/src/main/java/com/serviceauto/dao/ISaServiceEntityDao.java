package com.serviceauto.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.ServiceEntity;

public interface ISaServiceEntityDao extends JpaRepository<ServiceEntity, Long> {

}
