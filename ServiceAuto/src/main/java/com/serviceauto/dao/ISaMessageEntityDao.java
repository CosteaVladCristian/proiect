package com.serviceauto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.MessageEntity;

public interface ISaMessageEntityDao extends JpaRepository<MessageEntity, Long> {

	@Query(name = "MessageEntity.getSaAllUnreadMessages")
	List<MessageEntity> getSaAllUnreadMessages(@Param("sender") String sender);

}
