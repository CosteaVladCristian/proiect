package com.serviceauto.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.DefectionEntity;

public interface ISaDefectionEntityDao extends JpaRepository<DefectionEntity, Long> {

}
