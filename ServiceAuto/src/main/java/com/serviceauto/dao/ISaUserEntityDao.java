package com.serviceauto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.UserEntity;



public interface ISaUserEntityDao extends JpaRepository<UserEntity, Long> {

	@Query(name = "UserEntity.getAllUser")
	public List<UserEntity> getAllUser(@Param(value = "serviceId") long id);

	@Query(name = "UserEntity.login")
	public UserEntity login(@Param(value = "email") String email, @Param(value = "password") String password);
}
