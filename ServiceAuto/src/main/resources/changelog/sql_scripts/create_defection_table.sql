CREATE TABLE DEFECTION_TABLE 
(
ID BIGINT(20) NOT NULL AUTO_INCREMENT,
DESCRIPTION VARCHAR(200) NULL DEFAULT NULL,
APPOINTMENT_DATA BIGiNT(20) NULL DEFAULT NULL,
CAR_ID BIGINT(20) NULL DEFAULT NULL,
PRIMARY KEY(ID),
CONSTRAINT FK_SERVICE_DEFECTION_CONSTRAINT FOREIGN KEY (CAR_ID) REFERENCES CAR_TABLE(ID)
)